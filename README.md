# mapita

La interfaz del proyecto **Onta**. Diseñara para su uso en web, dando gran
importancia a que funcione en celulares. Posiblemente crear una web app
progresiva sea una buena idea.

## Desarrollo

``` bash
# Instalar dependencias
npm install

# iniciar servidor de desarrollo
npm run serve
```

import { fromLonLat } from 'ol/proj';
import { MAP_INITIAL_CENTER, MAP_INITIAL_ZOOM } from '@/settings';
import SearchBar from '@/components/SearchBar.vue';
import SettingsMenu from '@/components/SettingsMenu.vue';
import TrackBtn from '@/components/TrackBtn.vue';
import BusList from '@/components/BusList.vue';

export default {
  name: 'home',

  components: {
    SearchBar,
    SettingsMenu,
    TrackBtn,
    BusList,
  },

  data() {
    return {
      center: fromLonLat(MAP_INITIAL_CENTER),
      zoom: MAP_INITIAL_ZOOM,

      selectedBusRoutes: [],
      loading: false,
      settingsVisible: false,
      currentPosition: null,

      showStops: false,
      darkMode: false,

      // this flag is used to prevent the track button from disabling itself because it
      // moves the map, and we are using map movement to disable it :P
      isArtificialMovement: false,

      pixelRatio: window.devicePixelRatio,
    };
  },

  computed: {
    layerParams() {
      if (this.selectedBusRoutes.length === 0) {
        return { CQL_FILTER: "created_at >= '2018-01-01'" };
      }

      const ids = this.selectedBusRoutes.map(route => route.id.split('.')[1]).join(', ');

      return { CQL_FILTER: `id in (${ids})` };
    },
  },

  methods: {
    mapClick(event) {
      this.selectedBusRoutes = [];
      this.loading = true;

      const source = this.$refs.bus_source;

      const featureInfoUrl = source.getFeatureInfoUrl(
        event.coordinate,
        this.$refs.view.viewResolution,
        'EPSG:3857',
        {
          INFO_FORMAT: 'application/json',
          feature_count: 100,
        },
      );

      fetch(featureInfoUrl)
        .then(response => response.json())
        .then((json) => {
          this.selectedBusRoutes = json.features;
        })
        .catch(() => {
          this.loading = false;
        });
    },

    mapMoveStart() {
      if (!this.isArtificialMovement) {
        this.$refs.track.clearWatch();
      }
    },

    mapMoveEnd() {
      this.isArtificialMovement = false;
    },

    removeSelectedBusRoute(busId) {
      this.selectedBusRoutes = this.selectedBusRoutes.filter(route => route.id !== busId);
    },

    currentPositionSet(position) {
      this.currentPosition = fromLonLat(position.pos);
      this.isArtificialMovement = true;
      this.center = this.currentPosition;
      this.zoom = 14;
    },
  },
};

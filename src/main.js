import Vue from 'vue';
import './registerServiceWorker';

import {
  Map, TileLayer, OsmSource, XyzSource, WmsSource, Feature, PointGeom, StyleBox, IconStyle,
} from 'vuelayers';
import 'vuelayers/lib/style.css';

import App from './App.vue';
import router from './router';

Vue.use(Map);
Vue.use(TileLayer);
Vue.use(OsmSource);
Vue.use(XyzSource);
Vue.use(WmsSource);
Vue.use(Feature);
Vue.use(PointGeom);
Vue.use(StyleBox);
Vue.use(IconStyle);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
